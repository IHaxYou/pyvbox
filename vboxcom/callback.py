import sys
import time
import logging
import traceback
from . import atexit
from . import virtualbox
from .misc import _gen_uuid

#** Variables **#
log = logging.getLogger("callback")
log.setLevel(logging.DEBUG)
_ch = logging.StreamHandler(sys.stdout)
_ch.setLevel(logging.DEBUG)
_ch.setFormatter(logging.Formatter('%(asctime)s (%(name)s) %(levelname)-8s %(message)s'))
log.addHandler(_ch)

#** Classes **#
class Callback(object):
    """
    Object used for callback management.

    Allows for both permanent and conditionally
    temporary use via the CallbackManager
    """

    def __init__(self, callback, max_use=0, max_per_min=0, expiration=0):
        # set variables
        self._cb = callback
        self.max_use = max_use
        self.max_per_min = max_per_min
        self.expiration = time.time() + expiration
        # management variables
        self.uses = 0
        self.last_used = time.time()
        self._isexpr = expiration > 0
        if max_per_min > 0:
            self._tick = 0

    def expired(self):
        """return True if callback has expired"""
        # if max-uses are being checked
        if self.max_use > 0:
            if self.uses >= self.max_use:
                log.debug("callback: %s, expired on max-use: %d" % (self, self.max_use))
                return True
        # if expiration date is checked
        if self._isexpr:
            if time.time() >= self.expiration:
                log.debug("callback: %s, expired on date: %s" % (self, self.expiration))
                return True
        # if max-uses-per-minute are being checked
        if self.max_per_min > 0:
            # if time-elapsed < 60 secs: update uses per min
            if time.time() - self.last_used < 60:
                self._tick += 1
                # if uses >= max-per-min then expire
                if self._tick >= self.max_per_min:
                    log.debug("callback: %s, expired on max-use/min: %d" % (self, self.max_per_min))
                    return True
            else:
                self._tick = 0
        # return False if none above are true
        return False

    def pre_callback(self, *args, **kwargs):
        """return True if callback should be executed"""
        return True

    def run_callback(self, *args, **kwargs):
        """run callback and update uses"""
        self.uses += 1
        self.last_used = time.time()
        self._cb(*args, **kwargs)


class CallbackManager:
    """
    Handler for all existing VirtualBox event callbacks
    for simplified interface and easy control

    also ensures all callbacks are closed before
    exiting
    """

    def __init__(self, vbox_instance):
        # variables
        self._vbox = vbox_instance
        self._callbacks = {}
        self._ids = {}
        self.__uuid__ = _gen_uuid()
        # ensure callbacks are removed on any error
        atexit.add_func(self._cleanup_callbacks)

    def _spawn_callback(self, event_type):
        """spawn callback handler for specific event-type"""
        def callback(*args, **kwargs):
            # iterate callbacks for specific type
            for cb in self._callbacks[event_type][:]:
                # check if callback should be run
                if cb.pre_callback(*args, **kwargs):
                    # delete callbacks if they have expired
                    if cb.expired():
                        self._callbacks[event_type].remove(cb)
                    # else attempt to run them and print traceback on failure
                    else:
                        try:
                            # delete callback if it returns False
                            out = cb.run_callback(*args, **kwargs)
                            if not out and out is not None:
                                self.remove_callback(cb, event_type)

                        except Exception as e:
                            log.error("Callback(%s) ERROR: %r" % (event_type, e))
                            log.error("traceback:\n"+traceback.format_exc()+"\n\n")
        return callback

    def _cleanup_callbacks(self):
        """atexit function to remove all callbacks"""
        if len(self._callbacks) > 0:
            print("CLEANUP: CallbackManager[%s] removing all callbacks!" % self.__uuid__)
        self.remove_all_callbacks()

    def add_callback(self, callback, event_type):
        """attempt to add callbacks under given event-type key"""
        # ensure types are correct
        assert(isinstance(event_type, virtualbox.library.VBoxEventType))
        if not isinstance(callback, Callback):
            assert(callable(callback))
            callback = Callback(callback)
        # add callback
        if event_type not in self._callbacks:
            # add specific event-type to callback map
            self._callbacks[event_type] = [callback]
            # register callback function for this type
            self._ids[event_type] = (self._vbox.event_source.register_callback(
                self._spawn_callback(event_type),
                event_type,
            ))
        else:
            self._callbacks[event_type].append(callback)

    def remove_callback(self, callback, event_type):
        """attempt to remove given callback or specific event-type"""
        # ensure types are correct
        assert (isinstance(callback, Callback))
        assert (isinstance(event_type, virtualbox.library.VBoxEventType))
        # remove callback
        if event_type in self._callbacks:
            # remove callback if available
            self._callbacks[event_type].remove(callback)
            # if there are no callbacks for the given type
            if len(self._callbacks[event_type]) == 0:
                # remove master callback using id saved in '_ids' dictionary
                virtualbox.events.unregister_callback(self._ids[event_type])
                self._ids.pop(event_type, None)

    def remove_all_callbacks(self):
        """reset and remove all callbacks from the system"""
        # attempt to remove callbacks
        for id in self._ids.values():
            virtualbox.events.unregister_callback(id)
        self._callbacks = {}
        self._ids = {}
