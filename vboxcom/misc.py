import sys
import signal
from uuid import uuid4
from . import virtualbox

#** Variables **#

# list of signals handled by AtExitManager
_signals = {
    signal.SIGTSTP: "SIGTSTP", # STOP (Ctrl-Z)
    signal.SIGTERM: "SIGTERM",
    signal.SIGQUIT: "SIGQUIT",
}

#** Functions **#
def _gen_uuid():
    """builds and returns uuid as string"""
    return str(uuid4())

#** Classes **#
class AtExitManager:
    """
    Runs AtExit functions so long as error or signal can be captured
    and ensures cleanup is executed before exiting the program
    """

    def __init__(self):
        self._queue = {}
        # add signal handler for specified signals
        for sig in _signals.keys():
            signal.signal(sig, self.signal_handler)

    def exitfunc(self):
        """custom exit hook to cleanup objects"""
        for _, func in self._queue.items():
            try:
                func()
            except virtualbox.library.OleErrorUnexpected:
                pass
            except Exception as e:
                print("AtExit Err: %r" % e)

    def add_func(self, func):
        """add function to exit callback"""
        uuid = _gen_uuid()
        self._queue[uuid] = func
        return uuid

    def remove_func(self, uuid):
        """remove function from exit callback"""
        self._queue.pop(uuid, None)

    def exception_hook(self, exctype, value, traceback):
        """custom excpetion hook for exit of program"""
        self.exitfunc()
        sys.__excepthook__(exctype, value, traceback)

    def signal_handler(self, signum, stack):
        """custom handler for signals to allow proper object closing"""
        print('\rCaught signal %s(%d), exiting...' % (_signals[signum], signum))
        self.exitfunc()
        raise SystemExit()

class VBoxEnum:
    """basic collection of all important virtualbox enums"""
    APICMode                          = virtualbox.library.APICMode
    AccessMode                        = virtualbox.library.AccessMode
    AdditionsFacilityClass            = virtualbox.library.AdditionsFacilityClass
    AdditionsFacilityStatus           = virtualbox.library.AdditionsFacilityStatus
    AdditionsFacilityType             = virtualbox.library.AdditionsFacilityType
    AdditionsRunLevelType             = virtualbox.library.AdditionsRunLevelType
    AdditionsUpdateFlag               = virtualbox.library.AdditionsUpdateFlag
    AudioCodecType                    = virtualbox.library.AudioCodecType
    AudioControllerType               = virtualbox.library.AudioControllerType
    AudioDriverType                   = virtualbox.library.AudioDriverType
    AuthType                          = virtualbox.library.AuthType
    AutostopType                      = virtualbox.library.AutostopType
    BIOSBootMenuMode                  = virtualbox.library.BIOSBootMenuMode
    BandwidthGroupType                = virtualbox.library.BandwidthGroupType
    BitmapFormat                      = virtualbox.library.BitmapFormat
    CPUPropertyType                   = virtualbox.library.CPUPropertyType
    CertificateVersion                = virtualbox.library.CertificateVersion
    ChipsetType                       = virtualbox.library.ChipsetType
    CleanupMode                       = virtualbox.library.CleanupMode
    ClipboardMode                     = virtualbox.library.ClipboardMode
    CloneMode                         = virtualbox.library.CloneMode
    CloneOptions                      = virtualbox.library.CloneOptions
    DataFlags                         = virtualbox.library.DataFlags
    DataType                          = virtualbox.library.DataType
    DeviceActivity                    = virtualbox.library.DeviceActivity
    DeviceType                        = virtualbox.library.DeviceType
    DhcpOpt                           = virtualbox.library.DhcpOpt
    DhcpOptEncoding                   = virtualbox.library.DhcpOptEncoding
    DirectoryCopyFlags                = virtualbox.library.DirectoryCopyFlags
    DirectoryCreateFlag               = virtualbox.library.DirectoryCreateFlag
    DirectoryOpenFlag                 = virtualbox.library.DirectoryOpenFlag
    DirectoryRemoveRecFlag            = virtualbox.library.DirectoryRemoveRecFlag
    DnDAction                         = virtualbox.library.DnDAction
    DnDMode                           = virtualbox.library.DnDMode
    Enum                              = virtualbox.library.Enum
    ExportOptions                     = virtualbox.library.ExportOptions
    FaultToleranceState               = virtualbox.library.FaultToleranceState
    FileAccessMode                    = virtualbox.library.FileAccessMode
    FileCopyFlag                      = virtualbox.library.FileCopyFlag
    FileOpenAction                    = virtualbox.library.FileOpenAction
    FileOpenExFlags                   = virtualbox.library.FileOpenExFlags
    FileSeekOrigin                    = virtualbox.library.FileSeekOrigin
    FileSharingMode                   = virtualbox.library.FileSharingMode
    FileStatus                        = virtualbox.library.FileStatus
    FirmwareType                      = virtualbox.library.FirmwareType
    FramebufferCapabilities           = virtualbox.library.FramebufferCapabilities
    FsObjMoveFlags                    = virtualbox.library.FsObjMoveFlags
    FsObjRenameFlag                   = virtualbox.library.FsObjRenameFlag
    FsObjType                         = virtualbox.library.FsObjType
    GraphicsControllerType            = virtualbox.library.GraphicsControllerType
    GuestMonitorChangedEventType      = virtualbox.library.GuestMonitorChangedEventType
    GuestMonitorStatus                = virtualbox.library.GuestMonitorStatus
    GuestMouseEventMode               = virtualbox.library.GuestMouseEventMode
    GuestSessionStatus                = virtualbox.library.GuestSessionStatus
    GuestSessionWaitForFlag           = virtualbox.library.GuestSessionWaitForFlag
    GuestSessionWaitResult            = virtualbox.library.GuestSessionWaitResult
    GuestUserState                    = virtualbox.library.GuestUserState
    HWVirtExPropertyType              = virtualbox.library.HWVirtExPropertyType
    HostNetworkInterfaceMediumType    = virtualbox.library.HostNetworkInterfaceMediumType
    HostNetworkInterfaceStatus        = virtualbox.library.HostNetworkInterfaceStatus
    HostNetworkInterfaceType          = virtualbox.library.HostNetworkInterfaceType
    ImportOptions                     = virtualbox.library.ImportOptions
    KeyboardHIDType                   = virtualbox.library.KeyboardHIDType
    KeyboardLED                       = virtualbox.library.KeyboardLED
    LockType                          = virtualbox.library.LockType
    MachineState                      = virtualbox.library.MachineState
    MediumFormatCapabilities          = virtualbox.library.MediumFormatCapabilities
    MediumState                       = virtualbox.library.MediumState
    MediumType                        = virtualbox.library.MediumType
    MediumVariant                     = virtualbox.library.MediumVariant
    MouseButtonState                  = virtualbox.library.MouseButtonState
    NATAliasMode                      = virtualbox.library.NATAliasMode
    NATProtocol                       = virtualbox.library.NATProtocol
    NetworkAdapterPromiscModePolicy   = virtualbox.library.NetworkAdapterPromiscModePolicy
    NetworkAdapterType                = virtualbox.library.NetworkAdapterType
    NetworkAttachmentType             = virtualbox.library.NetworkAttachmentType
    ParavirtProvider                  = virtualbox.library.ParavirtProvider
    PathStyle                         = virtualbox.library.PathStyle
    PointingHIDType                   = virtualbox.library.PointingHIDType
    PortMode                          = virtualbox.library.PortMode
    ProcessCreateFlag                 = virtualbox.library.ProcessCreateFlag
    ProcessInputFlag                  = virtualbox.library.ProcessInputFlag
    ProcessInputStatus                = virtualbox.library.ProcessInputStatus
    ProcessOutputFlag                 = virtualbox.library.ProcessOutputFlag
    ProcessPriority                   = virtualbox.library.ProcessPriority
    ProcessStatus                     = virtualbox.library.ProcessStatus
    ProcessWaitForFlag                = virtualbox.library.ProcessWaitForFlag
    ProcessWaitResult                 = virtualbox.library.ProcessWaitResult
    ProcessorFeature                  = virtualbox.library.ProcessorFeature
    Reason                            = virtualbox.library.Reason
    Scope                             = virtualbox.library.Scope
    ScreenLayoutMode                  = virtualbox.library.ScreenLayoutMode
    SessionState                      = virtualbox.library.SessionState
    SessionType                       = virtualbox.library.SessionType
    SettingsVersion                   = virtualbox.library.SettingsVersion
    StorageBus                        = virtualbox.library.StorageBus
    StorageControllerType             = virtualbox.library.StorageControllerType
    SymlinkReadFlag                   = virtualbox.library.SymlinkReadFlag
    SymlinkType                       = virtualbox.library.SymlinkType
    TouchContactState                 = virtualbox.library.TouchContactState
    USBConnectionSpeed                = virtualbox.library.USBConnectionSpeed
    USBControllerType                 = virtualbox.library.USBControllerType
    USBDeviceFilterAction             = virtualbox.library.USBDeviceFilterAction
    USBDeviceState                    = virtualbox.library.USBDeviceState
    VBoxEventType                     = virtualbox.library.VBoxEventType
    VFSType                           = virtualbox.library.VFSType
    VirtualSystemDescriptionType      = virtualbox.library.VirtualSystemDescriptionType
    VirtualSystemDescriptionValueType = virtualbox.library.VirtualSystemDescriptionValueType
