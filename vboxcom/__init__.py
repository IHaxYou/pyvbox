import os
import sys
import time
import virtualbox
from . import misc

#** Variables **#

# Quick Access to VirtualBox Enums
enums = misc.VBoxEnum()

# Master cleanup handler for library
atexit = misc.AtExitManager()
sys.excepthook = atexit.exception_hook

#** Exceptions **#
VBoxError = virtualbox.library.VBoxError

class FileNotFoundError(Exception):
    pass

class VMStateError(Exception):
    pass

class SessionLockedError(Exception):
    pass

#** Functions **#

def cleanup():
    """simplified access to master cleanup handler"""
    atexit.exitfunc()


def is_running(machine):
    """return true if vm is running"""
    return machine.state == enums.MachineState.running


def wait_unlocked(machine):
    """wait until machine recognizes it is unlocked"""
    while machine.session_state != enums.SessionState.unlocked:
        time.sleep(0.25)


def start_vm(machine, headless=True):
    """attempt to start given machine"""
    if machine.state == enums.MachineState.running:
        raise VMStateError("VM: " + machine.name + " is already running!")
    if headless:
        machine.launch_vm_process(type_p='headless')
    else:
        machine.launch_vm_process(type_p='gui')


def stop_vm(machine, session=None):
    """attempt to stop given machine"""
    # check if already stopped
    if machine.state == enums.MachineState.powered_off:
        raise VMStateError("VM: "+ machine.name + " is already off")
    # spawn session if not given one
    if session is None: session = Session()
    # if session is already locked
    if session.is_locked():
        # attempt to power down
        progress = session.console.power_down()
        progress.wait_for_completion(-1)
    else:
        # lock machine with session to power-down
        with session.lock_machine(machine, enums.LockType.shared):
            # attempt to power down
            progress = session.console.power_down()
            progress.wait_for_completion(-1)
        # wait to ensure unlocked
        session.wait_unlocked()


def get_machine_mediums(machine):
    """
    iterate machine controllers to collect valid mediums
    for given machine object and return mediums
    """
    out = []
    for controller in machine.storage_controllers:
        for i in range(controller.port_count):
            for k in range(controller.max_devices_per_port_count):
                try:
                    medium = machine.get_medium(controller.name, i, k)
                    if hasattr(medium, "name"):
                        out.append(medium)
                except:
                    pass
    return out


def export_vm(appliance, machine, output, vendor=None, version=None, description=None, manifest=False):
    """
    complete exportation of given vm using given parameters and include output
    """
    output = os.path.realpath(output)
    # ensure output directory exists
    if '.' in output.split('/')[-1]:
        dirloc = os.path.dirname(output)
    else:
        dirloc = output
    if not os.path.exists(dirloc):
        raise FileNotFoundError("NoSuchLocation: %s" % dirloc)
    # export appliance to machine
    app_desc = machine.export_to(appliance, output)
    # add description settings if given
    if vendor is not None:
        app_desc.add_description(virtualbox.library.VirtualSystemDescriptionType.vendor, vendor, vendor)
    if version is not None:
        app_desc.add_description(virtualbox.library.VirtualSystemDescriptionType.version, version, version)
    if description is not None:
        app_desc.add_description(virtualbox.library.VirtualSystemDescriptionType.description, description, description)
    # add manifest if specified
    options = []
    if manifest:
        options.append(virtualbox.library.ExportOptions.create_manifest)
    # begin exportation and return virtualbox-progress object to keep track of progress
    return appliance.write("ovf-1.0", options, os.path.realpath(output))


def import_vm(appliance, ova_file, name=None, memory=None, cpus=None):
    """
    complete import of specified ova file along with optional system specs
    """
    appliance.read(ova_file)
    description = appliance.virtual_system_descriptions[0]
    if name is not None: description.set_name(name)
    if cpus is not None: description.set_cpu(str(cpus))
    if memory is not None:
        if memory % 256 != 0:
            raise ValueError("memory-flag must be multiple of 256")
        description.set_memory(str(memory))
    # attempt to import ova using temp-file
    return appliance.import_machines()

#** Classes **#
class Session(virtualbox.library.ISession):
    """
    Wrapped VirtualBox.library.ISession object
    to allow for greater object safety and extended
    to ensure that the session is closed in some form
    regardless of state or error
    """

    def __copy__(self):
        raise AttributeError("Session SHOULD NOT BE COPIED!")

    def __deepcopy__(self, dummy={}):
        raise AttributeError("Session SHOULD NOT BE COPIED!")

    def __enter__(self):
        """dont allow context entrance without previous session lock"""
        if self.state != enums.SessionState.locked:
            raise RuntimeError("Session entered context-manager without active lock!")

    def __exit__(self, type, value, traceback):
        """unlock manager if already locked on context exit"""
        if type != None:
            print("CLEANUP: Session[%s] unlocked due to error!" % self.__uuid__)
        self.unlock_machine()

    def lock_machine(self, machine, lock=virtualbox.library.LockType.shared):
        """attempt to bind machine to session and complete lock"""
        # check if already locked
        if self.state == enums.SessionState.locked:
            raise SessionLockedError("This session is already locked!")
        # attempt to lock machine
        error = None
        for _ in range(10):
            try:
                machine.lock_machine(self, lock)
            except Exception as exc:
                error = exc
                time.sleep(1)
                continue
            else:
                break
        else:
            if error is not None:
                raise Exception("Failed to create clone - %s" % error)
        # update lock status if succeeded and add emergency handler
        self._atexit_id = atexit.add_func(self._ulm)
        # return self for context-manager
        return self

    def _ulm(self):
        """preserve original unlock function in shorter form"""
        if self.is_locked():
            print("CLEANUP: Session[%s] force-unlocked!" % self.__uuid__)
        super(Session, self).unlock_machine()

    def is_locked(self):
        """return true if session is locked"""
        return self.state == enums.SessionState.locked

    def wait_locked(self):
        """waits for lock status to be locked"""
        while self.state != enums.SessionState.locked:
            time.sleep(0.25)

    def wait_unlocked(self):
        while self.state != enums.SessionState.unlocked:
            time.sleep(0.25)

    def unlock_machine(self):
        """
        wrapper for unlock_machine in case of errors
        and extended to work with custom lock_machine method
        """
        if self.state != enums.SessionState.locked:
            raise SessionLockedError("This session has not been locked!")
        super(Session, self).unlock_machine()
        # update variable and remove emergency handler
        atexit.remove_func(self._atexit_id)

class VirtualBox(virtualbox.library.IVirtualBox):
    """
    wrapper for existing VirtualBox utility
    with additional functionality to allow
    for easier management and error handling
    """

    def get_machine_mediums(self, vm_name):
        """
        return a list of mediums attached to vm
        """
        machine = self.find_machine(vm_name)
        return get_machine_mediums(machine)

    def start_vm(self, vm_name, headless=True):
        """
        easy and automatic way to start a vm
        with slightly more control
        """
        machine = self.find_machine(vm_name)
        start_vm(machine, headless=headless)

    def stop_vm(self, vm_name):
        """
        easy and automatic way to stop a vm
        with slightly more control
        """
        machine = self.find_machine(vm_name)
        stop_vm(machine)

    def export_vm(self, vm_name, output, vendor=None, version=None, description=None, manifest=False):
        """
        easy and more complete way of exporting
        a vm (though a progress object is still returned)
        """
        machine = self.find_machine(vm_name)
        appliance = self.create_appliance()
        return export_vm(appliance, machine, output, vendor, version, description, manifest)

    def import_vm(self, ova_file, name=None, memory=None, cpus=None):
        """
        easy and more complete way of exporting
        a vm (though a progress object is still returned)
        """
        appliance = self.create_appliance()
        return import_vm(appliance, ova_file, name, memory, cpus)

# lazy imports
from vboxcom import callback # noqa: F401