from setuptools import setup

setup(
    name='vboxcom',
    version='1.0.0',
    packages=['vboxcom'],
    url='https://gitlab.com/IHaxYou/pyvbox/',
    license='MIT License',
    author='imgurbot12',
    author_email='',
    description='simple, easy to use python wrapper for VirtualBox COM-API',
    install_requires=[
        'pyvbox',
    ]
)
